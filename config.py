from lbconfig.api import *
#-----------------------------------------------------------------------------#
# Configuration
#-----------------------------------------------------------------------------#
name='training'
name_ws='data_node'
lbconfig_package(
 name=name,
 version='0.1', 
 default_prefix='out',
 default_targets=['lb-libraries', 'archive-ws-data_node']
)
depends_on( 
 logicblox_dep, 
 lb_web_dep
)
target('workspaces', ['archive-ws-data_node'  ])
#-----------------------------------------------------------------------------#
# LogiQl libraries
#-----------------------------------------------------------------------------#
core_modules = [
 'dims',
 'metrics'
]
services_modules = [
 'dims_tdx',
 'metrics_tdx'
]
ws_modules = [
 'data_node'
]
#-----------------------------------------------------------------------------#
# Dependency configuration that you should not have to mess with
#-----------------------------------------------------------------------------#
external_libs = {
 'lb_web' : '$(lb_web)',
 'lb_web_connectblox' : '$(lb_web)'
}
#-----------------------------------------------------------------------------#
# compiling LogiQl libraries
#-----------------------------------------------------------------------------#
for lib_name in core_modules:
 lb_library( name=lib_name, srcdir='src/core', deps=external_libs )
for lib_name in services_modules:
 lb_library( name=lib_name, srcdir='src/services/tdx_services', deps=external_libs )
for lib_name in ws_modules:
 lb_library( name=lib_name, srcdir='src', deps=external_libs )
#-----------------------------------------------------------------------------#
# Workspace
#-----------------------------------------------------------------------------#
ws_archive(name="data_node", libraries=['data_node'])
#-----------------------------------------------------------------------------#
# Install
#-----------------------------------------------------------------------------#
install_dir('$(build)/workspaces', 'workspaces')
#-----------------------------------------------------------------------------#
# Deploy
#-----------------------------------------------------------------------------#
rule(
 output='deploy',
 input=[],
 commands=[ '\
   mkdir -p $(prefix)/workspaces/%s; \
   tar xvfz $(build)/workspaces/%s.tgz -C $(prefix)/workspaces/%s/;\
   lb import-workspace %s $(prefix)/workspaces/%s/ --overwrite\
  ' % (name_ws, name_ws, name_ws, name_ws, name_ws)]
)
rule(
 output='load_data',
 input=[],
 commands=[ '\
   lb web-server unload-services;\
   lb web-server load-services;\
   lb web-client import http://localhost:8080/dims_tdx/product -i data/product_hierarchy.csv;\
   lb web-client import http://localhost:8080/dims_tdx/location -i data/location_hierarchy.csv;\
   lb web-client import http://localhost:8080/dims_tdx/calendar -i data/calendar_hierarchy.csv;\
   lb web-client import http://localhost:8080/metrics_tdx/forecast -i data/forecast.csv;\
   lb web-client import http://localhost:8080/metrics_tdx/sales -i data/sales.csv\
  ' ]
)
   
 