
#!/bin/bash
export rule  =
 output='deploy'
 input=[]
 commands=[ '\
   mkdir -p $(prefix)/workspaces/%s; \
   tar xvfz $(build)/workspaces/%s.tgz -C $(prefix)/workspaces/%s/;\
   lb import-workspace %s $(prefix)/workspaces/%s/ --overwrite\
  ' % (data_node, data_node,data_node, data_node, data_node)]
