#!/bin/bash
   cd Documents/training
   lb web-server list -s
   lb web-server unload-services
   lb web-server load-services
   lb web-server list -s
   lb web-client import http://localhost:8080/dims_tdx/product -i data/product_hierarchy.csv
   lb web-client import http://localhost:8080/dims_tdx/location -i data/location_hierarchy.csv
   lb web-client import http://localhost:8080/dims_tdx/calendar -i data/calendar_hierarchy.csv
   lb web-client import http://localhost:8080/metrics_tdx/forecast -i data/forecast.csv
   lb web-client import http://localhost:8080/metrics_tdx/sales -i data/sales.csv
